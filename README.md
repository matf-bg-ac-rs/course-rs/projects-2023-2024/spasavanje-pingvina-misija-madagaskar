#  Spasavanje pingvina: Misija Madagaskar 🐧❄️ <br>

## Opis igre
- Pingvin, u ulozi igrača, ima zadatak da prikupi ključ kako bi oslobodio svog prijatelja iz kaveza. Tokom puta do ključa, susreće se s raznim izazovima poput stražara i lemura u pokretu. Postoji opasnost od pada u rupu, ali i mogućnost dobijanja nagrade u vidu ukusne ribe. Svaki igrač ima
opciju da igra samostalno ili u partnerstvu. Ukoliko se odluči za solo avanturu, postoji mogućnost da se nađe na rang listi ako postigne impresivne rezultate. S druge strane, u tandemu s prijateljem, mogu se takmičiti ko će pre stići do cilja. <br>


## Demo snimak projekta 
- link: [Pingvini sa madagaskara: Misija Madagaskar](https://drive.google.com/file/d/1AksZUC1ECeq2tp3TGjptU9347zaM6cEw/view?usp=sharing) <br>

## Okruženje
- [![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-olivia)](https://www.qt.io/download) <br>


## Programski jezik
- [![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-red)](https://www.cplusplus.com/)  *C++20*  <br>
- [![qt6](https://img.shields.io/badge/Framework-Qt6-blue)](https://doc.qt.io/qt-6/)  *Qt6* <br>


## Korišćene biblioteke
- Qt >= 4
- Qt Multimedia


## Instalacija
- Preuzeti i instalirati [*Qt* i *Qt Creator*](https://www.qt.io/download).
- Ako je to potrebno,  nadograditi verziju C++ na C++20 <br>
- Ukoliko je potrebno instalirati Qt Multimedia biblioteku komandom $ *sudo apt install qtmultimedia5-dev*
- Ukoliko je to potrebno instalirati [Catch2](https://github.com/catchorg/Catch2)


## Preuzimanje i pokretanje :
- 1. U terminalu se pozicionirati u željeni direktorijum
- 2. Klonirati repozitorijum komandom: `$ git clone git@gitlab com:matf-bg-ac-rs/course-rs/projects-2023-2024 spasavanje-pingvina-misija-madagaskar.git`
- 3. Otvoriti okruzenje *Qt Creator* i u njemu otvoriti pingvini.pro fajl
- 4. Pritisnuti dugme *Run* u donjem levom uglu ekrana ili *CTRL+R* <br>
    ## Za pokretanje servera :
- 1. Pored pingvini.pro fajla, neophodno je otvoriti i server.pro fajl
- 2. Pokrenuti server desnim klikom na server.pro fajl i klikom na dugme *Run*
    ## Za igranje multiplayer partije na 2 računara:
- 1. Jedan korisnik pokreće server i aplikaciju na svom računaru
- 2. Drugi korisnik se povezuje na istu mrežu i pokreće samo aplikaciju
- 3. Oba korisnika unose lokalnu IPv4 adresu korisnika koji je pokrenuo server
- 4. Partiju pokreće korisnik koji je pokrenuo i server, a zatim se i drugi korisnik pridruzuje

   ## Članovi: <br>
 - <a href="https://gitlab.com/ivana_neskovic">Ivana Nešković 167/2019</a> <br>
 - <a href="https://gitlab.com/velickovicana">Ana Veličković 170/2019</a> <br>
 - <a href="https://gitlab.com/ivanavuckovic">Ivana Vučković 197/2019</a> <br>
 - <a href="https://gitlab.com/jelenamitrovic296">Jelena Mitrović 357/2020</a> <br>
 - <a href="https://gitlab.com/mpapovic27">Marija Papović 63/2019</a> <br>
