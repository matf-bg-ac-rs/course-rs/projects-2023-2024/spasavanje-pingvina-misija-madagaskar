# Slučaj upotrebe "Igranje multiplayer partije"

**Kratak opis:**

Nakon što je odabrana opcija za igranje multiplayer partije, oba igrača unose svoja imena i ip adresu servera i klikom na dugme START igra kreće.
Igra traje dok jedan od igrača ne dođe do kaveza. Nakon što brži igrač stigne do kaveza,ako je uzeo kljuc, njemu se prikazuje poruka da je pobedio, a drugom igraču poruka da je izgubio.
U slučaju da je igrača savladala prepreka ili je odustao, za njega se igra završava, a drugi igrač dobija poruku i može da nastavi sa igrom.

**Akteri:**
Dva igrača

**Preduslovi:**
Aplikacija je pokrenuta, otvoren je glavni meni i uspostavljena je konekcija između dva igrača.

**Postuslovi:**
Jedan igrač je pobedio ili nema pobedika.Izlaz iz aplikacije.

**Glavni tok:**
1. Igrač iz glavnog menija bira dugme START.
2. Igračima se omogućava da izaberu opciju multiplayer partije.
3. Prelazi se na slučaj upotrebe 'Uspostavljanje konekcije sa serverom'.
4. Igračima se omogućava da unesu svoja imena i ip adresu servera.
5. Prelazi se na slučaj upotrebe 'Igra'.
6. Kada se završi partija, uradi sledeće:
   * 6.1.  Ako je igrača savladala prepreka njemu se ispisuje poruka da je izgubio, a drugom igraču poruka da može da nastavi sa igrom 
   * 6.2.  Ako je igrač stigao do cilja, njemu se ispisuje poruka da je pobedio, a drugom igraču poruka da je izgubio
7. Izlaz iz aplikacije

**Alternativni tokovi:**
A1: Neočekivan izlaz iz aplikacije - Ukoliko neko od igrača izađe iz aplikacije pre nego što je igra završena, drugom igraču je omogućeno da završi partiju.

**Podtokovi:**
/

**Specijalni zahtevi:**
Neophodno je da imamo tačno dva igrača koji su povezani na isti server.