# Slučaj upotrebe "Igranje singleplayer partije"

## Kratak opis

Igrač iz glavnog menija klikom na dugme start ima opciju da bira igranje singleplayer partije i upiše svoje ime. Igra traje dok igrač ne stigne do cilja, nakon čega se vrši upis rezultata, pa se igraču prikazuje glavni meni, ili dok ga u igri ne savlada neka prepreka i igrač bude primoran da završi igru i vrati na glavni meni.

## Akteri

Jedan igrač

## Preduslovi

Aplikacija je pokrenuta i otvoren je glavni meni.

## Postuslovi

Aplikacija vraća igrača u glavni meni.

## Glavni tok

1. Igrač iz glavnog menija bira dugme za START.
2. Igraču se omogućava da izabere opciju SINGLE PLAYER.
3. Igraču se omogućava da upiše ime
4. Ukoliko je upisao ime:
- 4.1. Prelazi se na korak 5
- 4.2. Inače, ispisuje se poruka da upiše ime i prelazi na korak 3
5. Igrač pokreće partiju i prelazi na slučaj upotrebe "Igra".
6. Igrač završava igru i šalje se informacija o završenoj partiji
7. Ako je partija uspešno završava:
- 7.1. Igrač stigao do cilja i prelazi se na slučaj upotrebe "Čuvanje rezultata".
- 7.2 Inače je igrača savladala prepreka i prelazi se na korak 8.
8. Aplikacija prikazuje glavni meni.

## Alternativni tok

- **A1:** Neočekivan izlaz iz aplikacije. Igra se završava.

## Podtokovi

Nema specifičnih podtokova.

## Specijalni zahtevi
Nema specijalnih zahteva

## Napomena: /
