# Slučaj upotrebe "Igra (Game)"

## Kratak opis

Igrač započinje igru u svom prozoru od startne pozicije, savladava prepreke, a cilj mu je da u što kraćem roku dođe do ključa kako bi oslobodio Leu iz kaveza, a da pritom ne izgubi život. U opciji multiplayer, dva igrača se takmiče, svako u svom prozoru, a pobednik je onaj koji prvi dođe do ključa.

## Akteri

- Jedan igrač (single player) ili dva igrača (multiplayer)

## Preduslovi

- Igra je pokrenuta
- Igrač je upisao svoje ime

## Postuslovi

- Partija igre je završena sakupljanjem jednog ključa i dolaskom do kaveza ili je igrač (oba igrača u slučaju multiplayer) u svom prozoru izgubio život.

## Osnovni tok

1. Postavlja se scena sa preprekama i ribama i prikazuje se igrač.
2. Konstruiše se novi tajmer i prikazuje se na ekranu.
3. Sve dok igrač ne sakupi ključ i stigne do kaveza ili izgubi život, radi sledeće:
  - 3.1 Prikazuje se odgovarajući deo scene koji prati kretanje igrača.
  - 3.2 Provera se preklapanje pozicije igrača sa pozicijom prepreka na sceni.
     - 3.2.1 Ukoliko je došlo do preklapanja  sa čuvarom, tajmer se uvećava za određeni broj sekundi.
     - 3.2.2 Ukoliko je došlo do preklapanja sa rupom ili lemurem igrač umire, tajmer se postavlja na 0 i prelazi se na korak 4.
  - 3.3 Proverava se preklapanje pozicije igrača sa pozicijom ribe na sceni.
      3.3.1 Tajmer se smanjuje za odgovarajući broj sekundi.
  - 3.4 Ukoliko je korisnik pritisnuo neki taster, vrši se ažuriranje pozicije igrača na sceni.
     - 3.4.1 Ako je pritisnut taster za skok, vrši se tačno jedan skok i tada se igrač može pomeriti najviše jednom levo ili desnon     pritiskajući taster za levo ili desno.
     - 3.4.2 Ukoliko je igrač na podlozi, može se kretati levo ili desno neograničeni broj puta, pritiskajući odgovarajuće tastere
  - 3.5 Proverava se preklapanje pozicije igrača sa pozicijom ključa
     - 3.5.1 Ukoliko je došlo do preklapanja, igrač je ispunio uslov za kraj igre i može zavšiti igru odlaskom do kaveza
  - 3.6 Proverava se preklapanje pozicije igrača sa pozicijom kaveza
     - 3.6.1 Ukoliko je došlo do preklapanja i ispunjen je uslov 3.5.1, igrač je uspešno završio igru i prelazi se na korak 4
     - 3.6.2 Ukoliko je došlo do preklapanja i nije ispunjen uslov 3.5.1, igra se ne završava i prelazi se na 3.7
  - 3.7 Ažurira se vrednost tajmera.
4. Zaustavlja se štoperica.
5. Igra je okončana i vraća se konačno vreme, kao i informacija o ishodu igre
  

## Alternativni tokovi

- **Ime:** Neočekivani izlaz iz aplikacije
  - **Opis:** Ukoliko dođe do neočekivanog izlaza iz aplikacije, sva podešavanja i rezultati se poništavaju, a aplikacija završava rad. Slučaj upotrebe se prekida.

## Dodatne informacije

- Ukoliko je izabrana opcija multiplayer, osnovni tok za oba igrača je isti. Oba igrača igraju igru u svom prozoru i njihova igra ne zavisi od igre drugog igrača, ali krajnji ishod zavisi.
