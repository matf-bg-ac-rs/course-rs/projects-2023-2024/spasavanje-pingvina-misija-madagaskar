# Slučaj upotrebe „Čuvanje rezultata“

**Kratak opis:**
Po uspešnom završetku igre, aplikacija koristi ime koje je uneto pre početka igre i skladišti informacije o odigranoj igri.

**Akteri:**
- Igrač – uspešno je završio igru

**Preduslovi:**
- Aplikacija je pokrenuta i igrač je prešao nivo uspešno

**Postuslovi:**
- Informacije o odigranoj igri su trajno sačuvane

**Osnovni tok:**
1. Igrač iz glavnog menija bira dugme za START. 
2. Proverava se koju opciju je izabrao. 
    - 2.1 Ako je igrač izabrao opciju singleplayer, prelazi se na slučaj upotrebe Igra. 
    - 2.2 Ukoliko je vreme različito od 0, proverava se da li je rezultat trenutnog igrača bolji od rezultata nekog drugog igrača koji se nalazi na trenutnoj rang listi. 
        - 2.2.1 Ako je taj uslov ispunjen, onda se ažurira rang lista. 
        - 2.2.2 Ako uslov nije ispunjen, skače se na 2.3. 
    - 2.3 Prikazuje se glavni meni. 

**Alternativni tokovi:**
- Neočekivani izlaz iz aplikacije. Ako u bilo kom koraku korisnik isključi aplikaciju, sve eventualno zapamćene informacije o trenutnoj igri se poništavaju i aplikacija završava rad. Slučaj upotrebe se završava.

**Podtokovi:** /
**Specijalni uslovi:** /
**Dodatne informacije:** /
