# Slučaj upotrebe "Podešavanje muzike za igru”

**Kratak opis:**
Igrač iz glavnog menija bira opciju Settings. Izborom te opcije otvara se 	prozor gde 	igrač može da uključi/isključi muziku kao i da podesi njenu 	jačinu.
 

**Akteri:**
Igrač – bira odgovarajuća podešavanja.


**Preduslovi:**
Aplikacija je pokrenuta i prikazan je glavni meni.


**Postuslovi:**
Izabrana podešavanja su sačuvana i mogu se promeniti samo u slučaju povratka u glavni 	meni.

**Glavni tok:**

    1. Igrač bira dugme Podešavanja iz glavnog menija.
	2. Aplikacija prikazuje prozor sa opcijama koje se mogu podesiti.
	3. Ako je igrac izabrao opciju On
	    3.1. Ako podesi jačinu zvuka
			3.1.1. Primenjuje se ta jačina do kraja igre.
		3.2. Primenjuje se podrazumevana jačina do kraja igre.
	4. Ako je igrač izabrao opciju Off
		4.1. Izabrana opcija se primenjuje do kraja igre.
	5. Igrač bira dugme OK za povratak u glavni meni.


**Alternativni tokovi:**
A1: Neočekivani izlaz iz aplikacije. Ako korisnik isključi aplikaciju ptokom podešavanja 	igre, sva izabrana podešavanja se brišu i aplikacija zavrsšava rad. Slučaj upotrebe se 	završava.

**Podtokovi:** /

**Specijalni zahtevi:** /

**Dodatne informacije:** /
