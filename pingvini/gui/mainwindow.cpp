#include "./mainwindow.h"
#include "ui_mainwindow.h"

#include "../game/player1.h"
#include "../game/level.h"
#include "../game/game.h"
#include "../game/client.h"
#include "../game/multiplayer.h"

Game *game;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),mSetter(new MusicSetter()),rangList(new RangList()),onePlayer(false)
{
    ui->setupUi(this);

    ui ->stackedWidget ->setCurrentIndex(0);

    this ->setWindowTitle("PINGVINI SA MADAGASKARA");
    this->setFixedSize(800, 600);

    bSetter = new backgroundSetter(this, ":/resources/backgroundd.jpg");
    bSetter->setNewBackground();

    QString fp = rangList->getFilePath();
    QFileSystemWatcher watcher;
    watcher.addPath(fp);

    connect(&watcher,SIGNAL(fileChanged(QString)),this,SLOT(showResoults()));
    connect(ui->pbStartOne, SIGNAL(clicked()), this, SLOT(startStopwatch()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete mSetter;
    delete rangList;

    if(game){
        delete game;
    }
}

void MainWindow::setLabelText(const QString &text)
{
    ui->lMessageServer->setText(text);
}

bool MainWindow::isOnePlayer()
{
    return onePlayer;
}

void MainWindow::disableStartButton()
{
    ui->pbStartTwoPlayers->setEnabled(false);
}

void MainWindow::on_pbClose_clicked()
{
    close();
}

void MainWindow::on_pbStart_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(3);

    this ->setWindowTitle("PODACI O IGRI");
    this->setFixedSize(800, 600);

    bSetter = new backgroundSetter(ui->page3Names, ":/resources/nameSurname.jpeg");
    bSetter->setNewBackground();

    connect(ui->pbStart, &QPushButton::clicked, bSetter, &backgroundSetter::setNewBackground);
}

void MainWindow::on_pbAbout_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(1);

    this ->setWindowTitle("ABOUT GAME");
    this->setFixedSize(800, 600);

    bSetter= new backgroundSetter(ui->page2AboutGame, ":/resources/aboutGamePingvini.jpeg");
    bSetter->setNewBackground();

    connect(ui->pbAbout, &QPushButton::clicked, bSetter, &backgroundSetter::setNewBackground);
}

void MainWindow::on_pbOK_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(0);
}

void MainWindow::on_pbBack_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(0);

}

void MainWindow::on_pbStart2_clicked()
{
    if(ui->rbOnePlayer->isChecked())
    {
        ui->stackedWidget->setCurrentIndex(2);
        bSetter = new backgroundSetter(ui->page4Game, ":/resources/nameSurname.jpeg");
        bSetter->setNewBackground();
        onePlayer= true;

        connect(ui->pbStart, &QPushButton::clicked, bSetter, &backgroundSetter::setNewBackground);
    }else if(ui->rbTwoPlayers->isChecked())
    {
        ui->stackedWidget->setCurrentIndex(6);
        this->setFixedSize(800, 600);

        bSetter = new backgroundSetter(ui->page7GameMultiplayer, ":/resources/nameSurname.jpeg");
        bSetter->setNewBackground();
        connect(ui->pbStart, &QPushButton::clicked, bSetter, &backgroundSetter::setNewBackground);
        onePlayer= false;
    }
}

void MainWindow::on_pbSettings_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(4);
    this->setFixedSize(800, 600);
}

void MainWindow::on_rbOnMusic_toggled(bool checked)
{
    if(checked)
    {
        mSetter->onMusic();
    }
}

void MainWindow::on_rbOffMusic_toggled(bool checked)
{
    if(checked)
    {
        mSetter->offMusic();
    }
}

void MainWindow::on_hsVolume_sliderMoved(int position)
{
    mSetter->setVolume(position);
    //ovo ne radi kako treba, treba popraviti ili izbrisati
}

void MainWindow::on_pbOkMusic_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);

}

void MainWindow::showResoults()
{
    QStringList currentRow;
    QStringList rows = rangList->getData();
    for(int i=0;i<10 && i< rows.size()-1;i++)
    {
        currentRow=rows.at(i).split(" ");
        for(int j=0;j<2;j++)
        {
            ui->tbRangList->setItem(i,j,new QTableWidgetItem(currentRow[j].trimmed()));
        }
    }
}

void MainWindow::on_pbRangList_clicked()
{
    ui->stackedWidget->setCurrentIndex(5);
    this->setFixedSize(800, 600);
    showResoults();
}

void MainWindow::on_rbOnePlayer_clicked()
{
    ui->rbTwoPlayers->setChecked(false);
    ui->rbOnePlayer->setChecked(true);
}

void MainWindow::on_rbTwoPlayers_clicked()
{
    ui->rbOnePlayer->setChecked(false);
    ui->rbTwoPlayers->setChecked(true);
}

void MainWindow::on_pbStartOne_clicked()
{
    QString userInput = ui->teOnePlayer->toPlainText();
    userInput = userInput.trimmed();

    if (!userInput.isEmpty())
    {
        QStringList lines = userInput.split(' ');

        lines.removeAll(QString());

        if (!lines.isEmpty())
        {
            QString name1 = lines.first().trimmed();
            game = new Game(name1, this);
            game->show();
            game->setFixedSize(800,600);
            ui->lMessage->setText("");
            hide();

        } else
        {
            ui->lMessage->setText("You did not enter a name!");
        }
    } else
    {
        ui->lMessage->setText("You did not enter a name!");
    }
}

void MainWindow::on_pbBackOne_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(3);
    ui->lMessage->setText("");
}

void MainWindow::on_pbBackRL_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::startStopwatch()
{
    stopwatch_.start();
}

void MainWindow::returnToMainMenu()
{
    ui->stackedWidget->setCurrentIndex(0);
    show();
}

void MainWindow::on_pbBackTwoPlayers_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(3);
    ui->lMessage_2->setText("");
}

void MainWindow::on_pbStartTwoPlayers_clicked()
{
    QString userInput = ui->teTwoPlayers->toPlainText();
    userInput = userInput.trimmed();

    QString ipAdress = ui->tEIPadresa->toPlainText();
    ipAdress = ipAdress.trimmed();

    if (!userInput.isEmpty() && !ipAdress.isEmpty())
    {
        QStringList lines = userInput.split(' ');
        QStringList linesIpAdress = ipAdress.split(' ');

        lines.removeAll(QString());
        linesIpAdress.removeAll(QString());

        if (!lines.isEmpty() && !linesIpAdress.isEmpty())
        {
            QString name1 = lines.first().trimmed();
            QString ipAdress1 = linesIpAdress.first().trimmed();

            if (!Client::indFirst || !Client::indSecond)
            {
                game = new Multiplayer(name1,ipAdress1, this);
                qDebug() << "cekamo drugog!";

                QTimer *timer = new QTimer(this);
                connect(timer, &QTimer::timeout, this, [this, name1, timer]()
                        {
                    if (Client::indFirst && Client::indSecond)
                    {
                        game->show();
                        ui->lMessage_2->setText("");
                        hide();

                        timer->stop();
                        delete timer;
                        game->setFixedSize(800,600);
                    }
                });

                timer->start(100);
            }
        }else
        {
            ui->lMessage_2->setText("You did not enter a name or IP adress!");
        }
    }else
    {
        ui->lMessage_2->setText("You did not enter a name or IP adress!");
    }
}

void MainWindow::on_pbHowToPlay_clicked()
{
    ui->stackedWidget->setCurrentIndex(7);
    bSetter = new backgroundSetter(ui->page8HowToPlay, ":/resources/howToPlay.png");
    bSetter->setNewBackground();


}




void MainWindow::on_pbGotIt_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

