#include "backgroundSetter.h"

backgroundSetter::backgroundSetter(QWidget *widget, const QString &imagePath)
    :QObject(widget), targetWidget(widget)
{

    if (!imagePath.isEmpty())
    {
        backgroundPixmap.load(imagePath);

        if (backgroundPixmap.isNull())
        {
            errorMessage = "Failed to load image";
            return;
        }

        backgroundPixmap = backgroundPixmap.scaled(targetWidget->size(), Qt::IgnoreAspectRatio);
    } else
    {
        qDebug() << "Putanja do slike je prazna";
    }

}

void backgroundSetter::setNewBackground()
{

    QPalette palette;
    palette.setBrush(QPalette::Window, backgroundPixmap);

    targetWidget->setAutoFillBackground(true);
    targetWidget->setPalette(palette);

    targetWidget->setFixedSize(targetWidget->size());
}

const QString &backgroundSetter::getErrorMessage() const
{
    return errorMessage;
}
