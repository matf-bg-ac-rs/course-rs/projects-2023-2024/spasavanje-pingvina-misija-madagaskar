#ifndef BACKGROUNDSETTER_H
#define BACKGROUNDSETTER_H


#include <QObject>
#include <QPixmap>
#include <QWidget>
#include<QDebug>

class backgroundSetter: public QObject
{
    Q_OBJECT
public:
    backgroundSetter(QWidget *widget, const QString &imagePath);

public slots:
    void setNewBackground();
    const QString &getErrorMessage() const;

private:
    QWidget *targetWidget;
    QPixmap backgroundPixmap;
    QString errorMessage;
};

#endif // BACKGROUNDSETTER_H
