#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QSoundEffect>
#include <QTimer>
#include <QGraphicsScene>
#include <QGraphicsView>
#include<QFileSystemWatcher>
#include<QDebug>

#include "../game/stopwatch.h"
#include"../gui/musicSetter.h"
#include"../game/rangList.h"
#include "../gui/backgroundSetter.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setLabelText(const QString &text);
    void disableStartButton();
    bool isOnePlayer();

private slots:
    void on_pbClose_clicked();
    void on_pbStart_clicked();
    void on_pbAbout_clicked();
    void on_pbSettings_clicked();
    void on_pbRangList_clicked();
    void on_pbOK_clicked();
    void on_pbBack_clicked();
    void on_pbStart2_clicked();
    void on_rbOnMusic_toggled(bool checked);
    void on_rbOffMusic_toggled(bool checked);
    void on_hsVolume_sliderMoved(int position);
    void on_pbOkMusic_clicked();
    void on_rbOnePlayer_clicked();
    void on_rbTwoPlayers_clicked();
    void on_pbStartOne_clicked();
    void on_pbBackOne_clicked();
    void on_pbBackRL_clicked();
    void on_pbBackTwoPlayers_clicked();
    void on_pbStartTwoPlayers_clicked();
    void on_pbHowToPlay_clicked();

    void startStopwatch();

    void showResoults();

    void on_pbGotIt_clicked();

public slots:
     void returnToMainMenu();

private:
    Ui::MainWindow *ui;
    stopwatch stopwatch_;
    MusicSetter *mSetter;
    RangList *rangList;
    backgroundSetter *bSetter;
    bool onePlayer;
};
#endif // MAINWINDOW_H
