#ifndef MUSICSETTER_H
#define MUSICSETTER_H
#include<QSoundEffect>

class MusicSetter
{
public:
    MusicSetter();
    ~MusicSetter();
    void onMusic();
    void offMusic();
    bool getIsMusic();
    void setVolume(int position);
    void addSoundEffect(const QString& filePath);
    double scaleVolume(int originalValue);


private:
    QSoundEffect *music;
    bool isMusic;
};

#endif // MUSICSETTER_H
