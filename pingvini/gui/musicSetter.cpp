#include "../gui/musicSetter.h"

MusicSetter::MusicSetter() : music(new QSoundEffect()),isMusic(false){}


bool MusicSetter::getIsMusic(){
    return isMusic;
}

void MusicSetter::onMusic()
{
    music->setSource(QUrl::fromLocalFile(":/resources/PenguinsOfMadagascarSong.wav"));
    music->setLoopCount(QSoundEffect::Infinite);
    music->setVolume(50);

    if(!getIsMusic())
    {
        music->play();
        isMusic = true;
    }
}

void MusicSetter::offMusic()
{
    if(getIsMusic())
    {
        music->stop();
        isMusic = false;
    }
}

void MusicSetter::setVolume(int position)
{
    double valueScaled = scaleVolume(position);
    music->setVolume(valueScaled);
}


void MusicSetter:: addSoundEffect(const QString& filePath)
{
      music->setSource(QUrl::fromLocalFile(filePath));
      music->setVolume(50);

      if(!getIsMusic())
      {
        music->play();
        isMusic = true;
      }
}

double MusicSetter::scaleVolume(int originalValue)
{
      int minValue = 0;
      int maxValue = 99;

      double valueScaled = 1.0*(originalValue - minValue)/(maxValue*1.0 - minValue);
      return valueScaled;

}

MusicSetter::~MusicSetter()
{
      delete music;
}

