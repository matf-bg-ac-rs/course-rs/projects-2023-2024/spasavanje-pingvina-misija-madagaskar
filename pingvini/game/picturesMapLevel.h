#ifndef PICTURESMAPLEVEL_H
#define PICTURESMAPLEVEL_H

#include<QGraphicsPixmapItem>
#include <QMap>

#include "./mapitem.h"

class picturesMapLevel : public QGraphicsPixmapItem
{
public:
    picturesMapLevel();
    ~picturesMapLevel();

    QMap<char, QString> map;
    QMap<char, qreal> scalingFactor;
    QMap<char, qreal> yAxisDrop;

    void setUpMap();
    void setUpCharacter(char symbol);
    QString getPath(char symbol);
    qreal getScalingFactor(char symbol);
    qreal getYDrop(char symbol);
};


#endif // PICTURESMAPLEVEL_H
