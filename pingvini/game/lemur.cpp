#include "lemur.h"

Lemur::Lemur(QGraphicsItem *parent) : QObject(), QGraphicsPixmapItem(parent),map(new picturesMapLevel()),direction(1),distance(50),currentDistance(0)
{
    setPixmap(QPixmap(map->getPath('L')).scaled(QSize(50,50)));
}

void Lemur::move()
{
    qreal dx = direction * 2;

    currentDistance += qAbs(dx);
    if (currentDistance >= distance)
    {
        currentDistance = 0;
        direction = -direction;
    }

    setPos(x() + dx, y());
}

int Lemur::getDirection() const
{
    return direction;
}

qreal Lemur::getDistance() const
{
    return distance;
}

qreal Lemur::getCurrentDistance() const
{
    return currentDistance;
}

qreal Lemur::getX() const
{
    return pos().x();
}

Lemur::~Lemur(){
    if (map) {
        delete map;
        map = nullptr;
    }
}
