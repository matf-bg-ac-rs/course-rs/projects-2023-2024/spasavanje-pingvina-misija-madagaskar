#include "./picturesMapLevel.h"

picturesMapLevel::picturesMapLevel()
{
    setUpMap();
}

void picturesMapLevel::setUpMap()
{
    map['P'] = ":/resources/platform_resized4.png";
    map['L'] = ":/resources/lemur.png";
    map['C'] = ":/resources/kavez_resize.png";
    map['G'] = ":/resources/guard.png";
    map['H'] = ":/resources/holeNewest.png";
    map['K'] = ":/resources/key_resized.png";
    map['$'] = ":/resources/player_resized.png";
    map['F'] = ":/resources/fishResized.png";
}

QString picturesMapLevel::getPath(char symbol)
{
    return map.value(symbol);
}

void picturesMapLevel::setUpCharacter(char symbol)
{
    setPixmap(QPixmap(map[symbol]));
}

qreal picturesMapLevel::getScalingFactor(char symbol)
{
    return scalingFactor.value(symbol);
}

qreal picturesMapLevel::getYDrop(char symbol)
{
    return yAxisDrop.value(symbol);
}

picturesMapLevel::~picturesMapLevel(){}
