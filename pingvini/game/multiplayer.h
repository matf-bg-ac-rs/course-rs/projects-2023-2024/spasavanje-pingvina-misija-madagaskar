#ifndef MULTIPLAYER_H
#define MULTIPLAYER_H

#include <QMessageBox>
#include <QPushButton>
#include "client.h"
#include "game.h"
#include"../gui/musicSetter.h"

class Multiplayer : public Game
{
    Q_OBJECT
protected:
    void closeEvent(QCloseEvent *event) override;

public:
    explicit Multiplayer(QString namePlayer1, QString ipAdress1, MainWindow* mainWindow);
    ~Multiplayer();
    virtual void gameOver(const QString &messageText, const QString &soundEffectPath) override;
    virtual void gameSuccessfullyFinished() override;
    virtual void keyTaken() override;

private:
    QString namePlayer1;
    MainWindow* mainWindow;
    Client *client;
    MusicSetter *mSetter;
    QLabel *timerLabel;
    bool isKeyTaken;
    QString status;
    QString mssg;

    void showQuitGameDialog(const QString &messageText);
    void showInfoLabel(const QString &message);

signals:
    void sendMessageToClient(QString status);

public slots:
    void readMessageFromClient(QString message);
};

#endif // MULTIPLAYER_H

