#ifndef PLAYERPARAMS_H
#define PLAYERPARAMS_H

#include <QApplication>
#include <QScreen>

class PlayerParams
{
public:
    PlayerParams();
    ~PlayerParams();

    void scaleParams();
    void getScreenHeight();

public:
    qreal height;
    qreal width;
    qreal screenHeight;
    qreal gravity;
    qreal stepX;
    qreal stepY;
    qreal posX;
    qreal posY;

};

#endif // PLAYERPARAMS_H
