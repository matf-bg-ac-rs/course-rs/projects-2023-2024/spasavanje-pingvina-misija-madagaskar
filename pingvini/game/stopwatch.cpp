#include "stopwatch.h"

stopwatch::stopwatch(QObject *parent) : QObject(parent),timer(new QTimer(this))
{
    connect(timer, &QTimer::timeout, this, &stopwatch::update);
}

void stopwatch::start()
{
    timeElapsed = QTime(0, 0);
    timer->start(10);
}

void stopwatch::stop()
{
    timer->stop();
}

QTime stopwatch::getTimeElapsed() const
{
    return timeElapsed;
}

void stopwatch::decreaseTime(int iterations)
{
    int millisecondsToDecrease = iterations * 10;
    if (timeElapsed >= QTime(0, 0).addMSecs(millisecondsToDecrease))
    {
        timeElapsed = timeElapsed.addMSecs(-millisecondsToDecrease);
        emit timeChanged(timeElapsed);

    }else
    {
        qDebug() << "Vreme ne može biti negativno!";
    }
}

void stopwatch::increaseTime(int iterations)
{
    int millisecondsToIncrease = iterations * 10;
    timeElapsed = timeElapsed.addMSecs(millisecondsToIncrease);
    emit timeChanged(timeElapsed);
}

void stopwatch::update()
{
    timeElapsed = timeElapsed.addMSecs(10);
    emit timeChanged(timeElapsed);
}

stopwatch::~stopwatch()
{
    delete timer;
}
