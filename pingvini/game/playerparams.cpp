#include "./playerparams.h"

PlayerParams::PlayerParams()
{
    getScreenHeight();
    scaleParams();
}

void PlayerParams::scaleParams()
{
    height = screenHeight * 0.085;
    width = height * 0.4;
    stepX = 3.5;
    stepY = height * 0.1;
    gravity = 0.02 * stepY;
    posX = -1500;
    posY = 440;
}

void PlayerParams::getScreenHeight()
{
    QScreen *screen = QApplication::screens().at(0);
    screenHeight = screen->availableSize().height();
}

PlayerParams::~PlayerParams(){}
