#include "client.h"

bool Client::indFirst = false;
bool Client::indSecond = false;

Client::Client(QObject* parent): QObject(parent), socket(new QTcpSocket(this)), port(12345){}

bool Client::myConnect(QString ipAdress)
{
    socket->connectToHost(ipAdress, port);

    connect(socket, &QTcpSocket::readyRead, this, &Client::communication);
    connect(socket, &QTcpSocket::disconnected, this, &Client::disconnected);

    return socket->waitForConnected();
}

void Client::communication()
{
    QString message = socket->readAll();

    if(message == "Client 1")
    {
        indFirst = true;
    }else if(message == "Client 2")
    {
        indSecond = true;
    }
    else if(message == "Both are connected!" || message == "Client 2Both are connected!")
    {
        indFirst = true;
        indSecond = true;
    }
    else
    {
        emit sendMessage(message);
    }
}

void Client::disconnected()
{
    socket->deleteLater();
}

void Client::readMessage(QString msg)
{
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        QByteArray status = msg.toUtf8();
        socket->write(status);
        socket->waitForBytesWritten();
    }
}

Client::~Client()
{
    if (socket != nullptr) {
        delete socket;
        socket = nullptr;
    }
}
