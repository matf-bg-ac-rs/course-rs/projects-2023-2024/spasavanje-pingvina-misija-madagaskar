#ifndef FISH_H
#define FISH_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include"../gui/musicSetter.h"
#include "./picturesMapLevel.h"

class Fish : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Fish();
    void collect();
    ~Fish();
    MusicSetter* getMusicSetter();
    picturesMapLevel* getMapLevel();

private:
    MusicSetter *mSetter;
    picturesMapLevel *map;
};

#endif // FISH_H
