#ifndef MAPITEM_H
#define MAPITEM_H

#include <QGraphicsPixmapItem>
#include <QFile>
#include <QPixmap>
#include <QDebug>

class mapItem : public QGraphicsPixmapItem
{
public:
    mapItem(QString imgPath);
    QString getErrorMessage();
    QString imgPath;
    ~mapItem();

private:
    qint16 scaleFactorX = 50;
    qint16 scaleFactorY = 50;
    QPixmap pixmap;
    QString errorMessage;
};

#endif // MAPITEM_H
