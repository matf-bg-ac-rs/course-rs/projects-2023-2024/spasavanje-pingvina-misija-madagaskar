#include "rangList.h"

RangList::RangList() : rangListFilePath("../pingvini/res.txt"){}

QStringList RangList::getData()
{
    QFile file(rangListFilePath);
    QStringList rows;
    data.clear();
    rows.clear();

    if(file.open(QFile::ReadOnly))
    {
        data=file.readAll();
        rows =data.split("\n");
        file.close();
    }
    return rows;
}

void RangList::addToRangList(QString currentName, int currentTime)
{
    int pos = 0;
    QFile file(rangListFilePath);
    QStringList rows,row;
    data.clear();
    rows.clear();
    row.clear();

    if(!file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        qDebug() << file.errorString();
    }else
    {
        data=file.readAll();
        rows=data.split("\n");
    }
    for(int i=0;i<rows.size()-1;i++)
    {
        row =rows.at(i).split(" ");
        int time =row.at(1).toInt();
        if(currentTime>time)
        {
            pos=pos+1;
        }
    }

    QTextStream stream(&file);
    stream.seek(0);

    for(int i=0;i<pos;i++)
    {
        stream<<rows.at(i)<<"\n";
    }

    stream<<currentName <<" "<<currentTime <<"\n";

    for(int i=pos;i<rows.size()-1;i++)
    {
        stream<< rows.at(i)<<"\n";
    }

    file.close();
}

void RangList::setFilePath(const QString& filePath)
{
    rangListFilePath = filePath;
}

QString RangList::getFilePath() const
{
    return this->rangListFilePath;
}

RangList::~RangList(){}
