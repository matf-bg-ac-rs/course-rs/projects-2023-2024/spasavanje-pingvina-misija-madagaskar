#ifndef PLAYER1_H
#define PLAYER1_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QKeyEvent>
#include <QGraphicsScene>
#include<QDebug>
#include <QApplication>
#include "./lemur.h"
#include "./key.h"
#include "./fish.h"
#include "./playerparams.h"
#include "./mapitem.h"

class Player1 : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
    Player1();
    ~Player1();

    PlayerParams *params;
    void keyPressEvent(QKeyEvent * event);
    void keyReleaseEvent(QKeyEvent *event) override;
    void standOnPlatform(QGraphicsItem *tile);
    qreal getVelocityX() const;
    qreal getVelocityY() const;
     bool collisionWithGuard;

public slots:
    void advance(int phase);

signals:
    void gameOver(const QString &messageText, const QString &soundEffectPath);
    void keyTaken();
    void gameSuccessfullyFinished();
    void activeTimer();
    void decreaseTime();
    void increaseTime();

private:
    bool isOnGround = true;
    qreal velocityX = 0;
    qreal velocityY = 0;
    void detectCollision();
    void jump();
    void walk();
};

#endif // PLAYER1_H
