#ifndef LEMUR_H
#define LEMUR_H
#include <QGraphicsPixmapItem>
#include <QObject>
#include "picturesMapLevel.h"
#include "stopwatch.h"

class Lemur : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Lemur(QGraphicsItem *parent = nullptr);
    ~Lemur();

    int getDirection() const;
    qreal getDistance() const;
    qreal getCurrentDistance() const;
    qreal getX() const;
    picturesMapLevel *map;

public slots:
    void move();

private:
    int direction;
    qreal distance;
    qreal currentDistance;
};

#endif // LEMUR_H

