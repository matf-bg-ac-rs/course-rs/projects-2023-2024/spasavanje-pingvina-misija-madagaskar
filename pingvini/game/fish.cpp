#include "fish.h"

Fish::Fish() : QGraphicsPixmapItem(), mSetter(new MusicSetter()), map (new picturesMapLevel())
{   
    setPixmap(QPixmap(map->getPath('F')).scaled(QSize(50,50)));
}

void Fish::collect()
{
    mSetter->addSoundEffect(":/resources/exactly.wav");
    scene()->removeItem(this);
}

Fish::~Fish()
{
    if(mSetter)
    {
        delete mSetter;
        mSetter = nullptr;
    }
    if (map) {
        delete map;
        map = nullptr;
    }
}

MusicSetter* Fish::getMusicSetter(){
    return mSetter;
}
picturesMapLevel* Fish::getMapLevel(){
    return map;
}
