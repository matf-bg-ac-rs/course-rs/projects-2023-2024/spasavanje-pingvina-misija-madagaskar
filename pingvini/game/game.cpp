#include "game.h"

Game::Game(QString namePlayer1, MainWindow* mainWindow) :  scene(new QGraphicsScene()), mainWindow(mainWindow), namePlayerOne(namePlayer1),rangList(new RangList()),
    isKeyTaken(false)
{
    Level *level = new Level(scene, view);
    level->startLevel();
    setupGameElements();
    connectCollision();
}

void Game::setupGameElements()
{
    setSceneBackground();
    setView();
    initializeTimer();
    setInitialPlayerPosition();
    setUpTimerLabel();
}

void Game::setSceneBackground()
{
    scene->setSceneRect(0, 0, 1200, 600);
    setBackgroundBrush(QBrush(QImage(":/resources/newBackg.jpg")));
    setScene(scene);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void Game::setView()
{
    view = new QGraphicsView(scene);
    view->setSceneRect(0, 0, 600, 500);
    view->resize(600, 500);
}

void Game::initializeTimer()
{
    stopwatch_ = new stopwatch(this);
    QObject::connect(stopwatch_, &stopwatch::timeChanged, scene, &QGraphicsScene::advance);
    connect(stopwatch_, &stopwatch::timeChanged, this, &Game::viewChange);
}

void Game::setUpTimerLabel()
{
    timerLabel = new QLabel();
    QFont font("Monospace", 11, 3, true);
    timerLabel->setGeometry(10, 10, 100, 30);
    timerLabel->setAutoFillBackground(true);
    timerLabel ->move(10, 10);
    timerLabel->setFont(font);
    if(mainWindow->isOnePlayer())
    {
    timerLabel->show();
    scene->addWidget(timerLabel);
    }
    connect(stopwatch_, &stopwatch::timeChanged, this, &Game::updateTime);
    stopwatch_->start();
}

void Game::setInitialPlayerPosition()
{
    player = new Player1();
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();
    scene->addItem(player);

    scene->setFocus();
}

void Game::viewChange()
{
    int playerXPos = player->pos().x();
    if(playerXPos < 0)
    {
        scene->setSceneRect(qMax(-300,playerXPos+300), 0, playerXPos - 300, 600);

    }else if(playerXPos > 800)
    {
         scene->setSceneRect(playerXPos,100,view->viewport()->width(),view->viewport()->height()-100);
    }
    else
    {
        scene->setSceneRect(playerXPos - 300, 0, playerXPos + 300, 600);
    }

    int timerX = playerXPos - 10;
    int timerY = 10;
    timerLabel->move(timerX, timerY);
}

void Game::connectCollision()
{
    connect(player, &Player1::gameOver, this, &Game::gameOver);
    connect(player, &Player1::keyTaken, this, &Game::keyTaken);
    connect(player, &Player1::gameSuccessfullyFinished, this, &Game::gameSuccessfullyFinished);
    connect(player, &Player1::decreaseTime, this, &Game::decreaseTime);
    connect(player, &Player1::increaseTime, this, &Game::increaseTime);
}

void Game::updateTime(QTime newTime)
{
    QString timeString = newTime.toString("mm:ss");
    timerLabel->setText("Time: " + timeString);
}

void Game::gameOver(const QString &messageText, const QString &soundEffectPath)
{
    addSoundEffect(soundEffectPath);
    scene->removeItem(player);

    QMessageBox msgBox;
    QPushButton *returnButton = msgBox.addButton(tr("Return to Main Menu"), QMessageBox::ActionRole);
    msgBox.setText(messageText);
    msgBox.exec();

    QAbstractButton *clickedButton = msgBox.clickedButton();
    QPushButton *returnButtonCast = dynamic_cast<QPushButton*>(clickedButton);

    if (returnButtonCast && returnButtonCast == returnButton)
    {
        mainWindow->returnToMainMenu();
        close();
    }

}

void Game::addSoundEffect(const QString &QsoundEffectPath)
{
    stopwatch_->stop();
    mSetter = new MusicSetter();
    mSetter->offMusic();
    mSetter->addSoundEffect(QsoundEffectPath);
}

void Game::gameSuccessfullyFinished()
{
    QString msg;

    if(isKeyTaken)
    {
        addSoundEffect(":/resources/WinSoundEffect.wav");
        stopwatch_->stop();
        QTime time=stopwatch_->getTimeElapsed();
        int timeInSec = time.second() + time.minute()*60;
        rangList->addToRangList(namePlayerOne,timeInSec);

        QString msg  = "Your time is " + QString::number(timeInSec) + "s.Your result will be saved :-)";
        QMessageBox msgBox;
        QPushButton *returnButton = msgBox.addButton(tr("Return to main manu"), QMessageBox::ActionRole);
        msgBox.setText(msg);
        msgBox.exec();

        QAbstractButton *clickedButton = msgBox.clickedButton();
        QPushButton *returnButtonCast = dynamic_cast<QPushButton*>(clickedButton);

        if (returnButtonCast && returnButtonCast == returnButton)
        {
            mainWindow->returnToMainMenu();
            close();
        }
    }else
    {
        QPushButton *myButton = new QPushButton();
        msg = "It looks like you didn't take the key. :(";
        myButton = msgBox.addButton(QMessageBox::Ok);
        msgBox.setText(msg);
        msgBox.exec();
        player->collisionWithGuard = false;
        delete(myButton);
    }
}

void Game::keyTaken()
{
    isKeyTaken = true;
}

void Game::decreaseTime()
{
    stopwatch_ ->decreaseTime(100);
}

void Game::increaseTime()
{
    stopwatch_->increaseTime(200);
    mSetter = new MusicSetter();
    mSetter->addSoundEffect(":/resources/OhNoSoundEffect.wav");
}

stopwatch*  Game::getStopwatch()
{
    return stopwatch_;
}

QLabel* Game::getLabel()
{
    return timerLabel;
}

Game::~Game()
{
    if (player)
    {
        delete player;
        player = nullptr;
    }

    if (rangList)
    {
        delete rangList;
        rangList = nullptr;
    }

    if (stopwatch_)
    {
        delete stopwatch_;
        stopwatch_ = nullptr;
    }

    if (timerLabel)
    {
        delete timerLabel;
        timerLabel = nullptr;
    }

    if (mSetter)
    {
        delete mSetter;
        mSetter = nullptr;
    }

    if(scene)
    {
        delete scene;
        scene = nullptr;
    }

    if(view)
    {
        delete view;
        scene = nullptr;
    }
}
