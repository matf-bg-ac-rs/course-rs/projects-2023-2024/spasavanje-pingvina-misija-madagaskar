#include "./mapitem.h"

mapItem::mapItem(QString imagePath)
{

    if (QFile::exists(imagePath))
    {
        imgPath = imagePath;
        setPixmap(QPixmap(imagePath).scaled(QSize(120, 100), Qt::KeepAspectRatio));
    }
    else
    {
        errorMessage = "Failed to load image: " + imagePath;
        qDebug() << errorMessage;
    }
}

QString mapItem::getErrorMessage()
{
    return errorMessage;
}

mapItem::~mapItem(){}
