#include "key.h"

Key::Key() : QGraphicsPixmapItem(), mSetter(new MusicSetter()),map(new picturesMapLevel())
{
    setPixmap(QPixmap(map->getPath('K')).scaled(QSize(50,50)));
}

void Key::collect()
{
    mSetter->addSoundEffect(":/resources/exactly.wav");
    scene()->removeItem(this);
}

Key::~Key(){
    if(mSetter)
    {
        delete mSetter;
        mSetter = nullptr;
    }
    if (map) {
        delete map;
        map = nullptr;
    }
}
