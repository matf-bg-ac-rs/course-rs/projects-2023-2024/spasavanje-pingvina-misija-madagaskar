#ifndef KEY_H
#define KEY_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include "picturesMapLevel.h"
#include"../gui/musicSetter.h"

class Key : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
    Key();
    ~Key();
    void collect();

private:
    MusicSetter *mSetter;
    picturesMapLevel *map;
};

#endif // KEY_H
