#include "./player1.h"

Player1::Player1() : QGraphicsPixmapItem(),params(new PlayerParams()),collisionWithGuard(false)
{
    setPixmap(QPixmap(":/resources/pingvin1.png"));
    setPos(params->posX, params->posY);
}

void Player1::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Left:
        velocityX = -params->stepX;
        break;
    case Qt::Key_Right:
        velocityX = params->stepX;
        break;
    case Qt::Key_Up:
        if (isOnGround)
        {
            velocityY = -params->stepY;
            setPos(x(), y() + velocityY);
            isOnGround = false;
        }
        break;
    }
}

void Player1::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Left || event->key() == Qt::Key_Right)
    {
        velocityX = 0;
    }
}

void Player1::standOnPlatform(QGraphicsItem *tile)
{
    QRectF tileRect = tile->boundingRect();
    QPolygonF tileRectPoints = tile->mapToScene(tileRect);
    QPolygonF playerRectPoints = mapToScene(boundingRect());

    if (playerRectPoints[2].y() <= tileRectPoints[0].y() + 20)
    {
        isOnGround = true;
    } else if (!isOnGround)
    {
        qreal offset = 0;

        if (playerRectPoints[3].x() <= tileRectPoints[3].x() - 20)
        {
            offset = -10;
        } else if (playerRectPoints[2].x() >= tileRectPoints[2].x() + 20)
        {
            offset = 10;
        }

        setPos(x() + offset, y());

        if (!isOnGround && playerRectPoints[1].y() <= tileRectPoints[3].y() - 20)
        {
            velocityY = params->stepY / 2;
        }
    }
}

void Player1::advance(int phase)
{
    if(!phase)
    {
        return;
    }

    if(y() > params->posY + 50)
            emit gameOver("You lost. TRY AGAIN!", ":/resources/end_game.wav");

    walk();
    jump();
    detectCollision();

    emit activeTimer();
}

void Player1::detectCollision()
{
    QList<QGraphicsItem *> collidingItems_ = collidingItems();

    if(collidingItems_.size()){

        for (auto &collidingItem : collidingItems_)
        {
            if (dynamic_cast<Lemur*>(collidingItem))
            {
                emit gameOver("Oops, you were killed by a Lemur. TRY AGAIN!", ":/resources/end_game.wav");
            }
            else if (dynamic_cast<Key*>(collidingItem))
            {
                dynamic_cast<Key*>(collidingItem)->collect();
                collisionWithGuard = false;
                emit keyTaken();
            }
            else if (dynamic_cast<Fish*>(collidingItem))
            {
                dynamic_cast<Fish*>(collidingItem)->collect();
                emit decreaseTime();
            }
            else if(dynamic_cast<mapItem*>(collidingItem))
            {
                mapItem* item = (mapItem*)(collidingItem);
                QString imgPath = item->imgPath;

                if(imgPath == ":/resources/kavez_resize.png")
                {
                    setPos(x()-10, 340);
                    velocityX=0;
                    velocityY=0;
                    isOnGround = true;
                    collisionWithGuard = false;
                    emit gameSuccessfullyFinished();
                }else if(imgPath == ":/resources/guard.png")
                {
                    if(!collisionWithGuard)
                    {
                        collisionWithGuard = true;
                        emit increaseTime();
                    }
                }else if(imgPath == ":/resources/platform_resized4.png")
                {
                    this->standOnPlatform(collidingItem);
                }
                else if(imgPath == ":/resources/holeNewest.png")
                {
                    emit gameOver("You hit a hole... :(  TRY AGAIN!", ":/resources/LosingSoundEffect.wav");
                }
            }
        }
    }
    else
    {
        isOnGround= false;
    }
}

void Player1::jump()
{
    if(!isOnGround)
    {
        if(velocityY < 10)
        {
                velocityY += params->gravity;
        }
        setPos(x(), y() + velocityY);
    }
}

void Player1::walk()
{
    setPos(x() + velocityX, y());

    if(!isOnGround)
    {
        if(velocityY < 10)
        {
                velocityY += params->gravity;
        }
        setPos(x(), y() +velocityY);
    }

}

qreal Player1::getVelocityX() const
{
    return velocityX;
}

qreal Player1::getVelocityY () const
{
    return velocityY;
}

Player1::~Player1(){

    if (params != nullptr) {
        delete params;
        params = nullptr;
    }
}
