#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <ostream>
#include <QDebug>
#include <QHostAddress>
#include <QObject>
#include <QTcpSocket>

class Client : public QObject
{
    Q_OBJECT
public:
    Client(QObject* parent = nullptr);
    ~Client();
    bool myConnect(QString ipAdress);
    static bool indFirst;
    static bool indSecond;

public slots:
    void communication();
    void disconnected();

public slots:
    void readMessage(QString message);

signals:
    void sendMessage(QString message);

private:
    QTcpSocket* socket;
    int port;
};

#endif // CLIENT_H


