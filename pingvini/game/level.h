#ifndef LEVEL_H
#define LEVEL_H

#include <QtGlobal>
#include <QFile>
#include<QDebug>
#include <QGraphicsView>
#include <QPixmap>
#include <QUrl>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QWidget>
#include <QTextStream>

#include "./picturesMapLevel.h"
#include "./mapitem.h"
#include "./stopwatch.h"
#include "./key.h"
#include"./fish.h"
#include "game.h"

class Level : public QGraphicsView
{
public:
    Level();
    Level(QGraphicsScene* scene, QGraphicsView* view);
    ~Level();

    QGraphicsScene* scene;
    QGraphicsView* view;

    void startLevel();

private:
    stopwatch *stopwatch_;

    int sceneSizeX;
    int sceneSizeY;

    picturesMapLevel *map;

    void parseLevelMap();
    void addObject(char symbol, qreal x, qreal y);
};

#endif // LEVEL_H
