#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QLabel>
#include <QTimer>
#include <QGraphicsTextItem>
#include <QFont>
#include<QMessageBox>
#include<QApplication>
#include<QDebug>
#include <QPushButton>
#include <QTextStream>
#include <QTimer>
#include <QString>
#include <QVector>
#include <QMatrix2x2>

#include "./player1.h"
#include "./level.h"
#include "../game/stopwatch.h"
#include "./lemur.h"
#include "./key.h"
#include "./rangList.h"
#include "../gui/musicSetter.h"
#include "../gui/mainwindow.h"
#include "../game/fish.h"

class Game: public QGraphicsView
{
    Q_OBJECT
public:
    Game(QString namePlayer1, MainWindow* mainWindow);
    virtual ~Game();

    QGraphicsScene *scene;
    Player1 *player;
    QGraphicsView* view;

    const static int sceneSizeX = 3000;
    const static int sceneSizeY = 500;

private:
    MainWindow* mainWindow;
    QString namePlayerOne;
    RangList *rangList;
    stopwatch *stopwatch_;
    QLabel *timerLabel;
    bool isKeyTaken;
    MusicSetter *mSetter;
    bool *backToManu;
    QMessageBox msgBox;

    const static int viewWidth = 800;
    const static int viewHeight = 600;

    void initializeTimer();
    void setInitialPlayerPosition();
    void setUpTimerLabel();
    void connectCollision();

    void setupGameElements();

    void addSoundEffect(const QString &QsoundEffectPath);

public slots:
    void updateTime(QTime newTime);
    void viewChange();
    virtual void gameOver(const QString &messageText, const QString &soundEffectPath);
    virtual void keyTaken();
    void decreaseTime();
    void increaseTime();
    virtual void gameSuccessfullyFinished();
    stopwatch* getStopwatch();


    void setSceneBackground();
    void setView();

    QLabel* getLabel();

};


#endif // GAME_H
