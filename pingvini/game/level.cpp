#include "./level.h"

Level::Level() :  scene(nullptr), view(nullptr), stopwatch_(nullptr), sceneSizeX(0) , sceneSizeY(0) {}

Level::Level(QGraphicsScene *scene, QGraphicsView *view) : scene(scene),view(view), stopwatch_(nullptr), sceneSizeX(Game::sceneSizeX),
    sceneSizeY(Game::sceneSizeY), map(new picturesMapLevel())
{
  stopwatch_ = new stopwatch(this);
  stopwatch_->start();
}

void Level::startLevel()
{
    this->parseLevelMap();
}

void Level::parseLevelMap()
{
    QString filename = ":/resources/fajl.txt";
    QFile file(filename);
    if(!file.exists())
    {
        qDebug() << "File does not exists";
        return;
    }

    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Opening failed";
        return;
    }

    QTextStream in(&file);
    QStringList line = in.readLine().split(" ");
    qreal sizeX = line[0].toInt();
    qreal sizeY = line[1].toInt();

    for(int y = 0; y < sizeY; y++)
    {
        QString sceneObjects = in.readLine();
        for (int x = 0; x < sizeX; x++){
            addObject(sceneObjects[x].toLatin1(), x * sceneSizeX / sizeX - 1500, (y + 1)  * sceneSizeY / sizeY);
        }
    }

    file.close();
}

void Level::addObject(char symbol, qreal x, qreal y){
    //picturesMapLevel *map = new picturesMapLevel();

    if(symbol == 'L')
    {
        Lemur *lemur = new Lemur();
        lemur->setPos(x, y - map->getYDrop(symbol)+50);

        scene->addItem(lemur);
        QObject::connect(stopwatch_, &stopwatch::timeChanged, this, [=]() mutable {
            lemur->move();
        });

    }else if(symbol == 'K')
    {
        Key *key = new Key();
        key->setPos(x, y - map->getYDrop(symbol));
        scene->addItem(key);
    }else if(symbol == 'F')
    {
        Fish *fish = new Fish();
        fish->setPos(x, y - map->getYDrop(symbol));
        scene->addItem(fish);

    }else if (symbol == 'H')
    {
        mapItem *item = new mapItem(map->getPath(symbol));
        item->setPos(x, y - map->getYDrop(symbol)-67);
        scene->addItem(item);

    }else if(symbol != '-')
    {
        mapItem *item = new mapItem(map->getPath(symbol));
        item->setPos(x, y - map->getYDrop(symbol));
        scene->addItem(item);
    }
}

Level::~Level(){
    delete stopwatch_;
    delete map;
}
