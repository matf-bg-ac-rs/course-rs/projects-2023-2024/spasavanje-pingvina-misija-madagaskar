#include "multiplayer.h"

Multiplayer::Multiplayer(QString namePlayer1,QString ipAdress1, MainWindow* mainWindow)
    : Game(namePlayer1, mainWindow), namePlayer1(namePlayer1), mainWindow(mainWindow), client(new Client()), mSetter(new MusicSetter())
{
    if(client->myConnect(ipAdress1))
    {
        mainWindow->disableStartButton();
        mainWindow->setLabelText("Successful connection! Wait for another player...");
    }
    else
    {
        mainWindow->setLabelText("Connection failed for the entered IP address!");
    }

    connect(this,&Multiplayer::sendMessageToClient,client,&Client::readMessage);
    connect(client,&Client::sendMessage,this,&Multiplayer::readMessageFromClient);
}

void Multiplayer::gameOver(const QString &messageText, const QString &soundEffectPath)
{
    mSetter->offMusic();
    mSetter->addSoundEffect(soundEffectPath);
    scene->removeItem(player);

    status = "false";
    emit sendMessageToClient(status);

    showQuitGameDialog(messageText);
}

void Multiplayer::gameSuccessfullyFinished()
{
    QMessageBox msgBox;

    if(isKeyTaken)
    {
        status = "true";
        mssg  = "You WIN";
        emit sendMessageToClient(status);
        showQuitGameDialog(mssg);
    }
    else
    {
        QPushButton *myButton = new QPushButton();
        mssg = "It looks like you didn't take the key. :(";
        myButton = msgBox.addButton(QMessageBox::Ok);
        msgBox.setText(mssg);
        msgBox.exec();
        player->collisionWithGuard = false;
        delete(myButton);
    }
}

void Multiplayer::keyTaken()
{
    isKeyTaken = true;
}

void Multiplayer::closeEvent(QCloseEvent *event)
{
    mssg = "second player has given up";
    emit sendMessageToClient(mssg);
    QWidget::closeEvent(event);
}

void Multiplayer::readMessageFromClient(QString message)
{
    if(message == "true")
    {
        mssg = "Second player is win";
        showQuitGameDialog(mssg);
    }else if(message == "false")
    {

        mssg = "Second player has lost, you can continue!";
        showInfoLabel(mssg);
    }
    else if( message == "second player has given up")
    {

        mssg = "Second player has given up, you can continue!";
        showInfoLabel(mssg);
    }
}

void Multiplayer::showQuitGameDialog(const QString &messageText)
{
    QMessageBox msgBox;
    QPushButton *returnButton = msgBox.addButton(tr("Quit game"), QMessageBox::ActionRole);
    msgBox.setText(messageText);

    msgBox.exec();

    QAbstractButton *clickedButton = msgBox.clickedButton();
    QPushButton *returnButtonCast = dynamic_cast<QPushButton*>(clickedButton);

    if (returnButtonCast && returnButtonCast == returnButton)
    {
        QApplication::quit();
    }
}

void Multiplayer::showInfoLabel(const QString &message)
{
    QLabel *infoLabel = new QLabel(message, this);
    infoLabel->setAlignment(Qt::AlignCenter);
    infoLabel->setGeometry(0, 0, 400, 200);
    infoLabel->move(width() / 2 - infoLabel->width() / 2, height() / 2 - infoLabel->height() / 2);
    infoLabel->show();

    QTimer::singleShot(3000, infoLabel, &QLabel::hide);
}

Multiplayer::~Multiplayer()
{
    if (client)
    {
        delete client;
        client = nullptr;
    }
    if(mSetter){
        delete mSetter;
        mSetter = nullptr;
    }
}
