#ifndef RANGLIST_H
#define RANGLIST_H
#include<QString>
#include <QFile>
#include <QDebug>

class RangList
{
public:
    RangList();
    ~RangList();
    QStringList getData();
    void addToRangList(QString name,int score);
    QString getFilePath() const;
    void setFilePath(const QString& filePath);

private:
    QString rangListFilePath;
    QString data;
};

#endif // RANGLIST_H
