#include "catch.hpp"
#include "../game/level.h"

TEST_CASE("startLevel", "[function]")
{
    SECTION("Provera postojanja fajla iz kog se cita izgled nivoa")
    {
        //Arrange
        QString path = "../resources/fajl.txt";
        QFile txt_file(path);

        //Act

        //Assert
        REQUIRE(txt_file.exists());
    }
}


TEST_CASE("Level inicijalizacija", "[Level]") {
    QGraphicsScene scene;
    QGraphicsView view(&scene);

    Level level(&scene, &view);

    SECTION("Scena i pogled su ispravno postavljeni") {
        REQUIRE(level.scene == &scene);
        REQUIRE(level.view == &view);
    }
}





