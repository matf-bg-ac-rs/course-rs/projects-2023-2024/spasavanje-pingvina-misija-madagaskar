#include "catch.hpp"
#include <QGraphicsScene>
#include "../game/key.h"

TEST_CASE("collect", "[function]")
{
    SECTION("Collect metod sklanja kljuc sa scene")
    {
        Key key;

        QGraphicsScene scene;
        scene.addItem(&key);
        key.collect();

        REQUIRE(scene.items().isEmpty());

    }
}
