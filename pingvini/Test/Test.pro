TEMPLATE = app
QT       += core gui widgets multimedia testlib

CONFIG += c++17

SOURCES += \
    backgroundSetterTest.cpp \
    fishTest.cpp \
    gameTest.cpp \
    keyTest.cpp \
    lemurTest.cpp \
    levelTest.cpp \
    main.cpp \
    ../game/player1.cpp \
    ../game/fish.cpp \
    ../game/game.cpp \
    ../game/key.cpp \
    ../game/lemur.cpp \
    ../game/level.cpp \
    ../game/mapitem.cpp \
    ../game/multiplayer.cpp \
    ../game/client.cpp \
    ../game/picturesMapLevel.cpp \
    ../game/playerparams.cpp \
    ../game/rangList.cpp \
    ../game/stopwatch.cpp \
    ../gui/backgroundSetter.cpp \
    ../gui/musicSetter.cpp \
    ../gui/mainwindow.cpp \
    mapItemTest.cpp \
    musicSetterTest.cpp \
    pictureMapLevelTest.cpp \
    player1Test.cpp \
    playerParamsTest.cpp \
    rangListTest.cpp \
    stopwatchTest.cpp

HEADERS += \
    catch.hpp \
    catch.hpp \
    ../game/player1.h \
    ../game/fish.h \
    ../game/game.h \
    ../game/key.h \
    ../game/lemur.h \
    ../game/level.h \
    ../game/mapitem.h \
    ../game/multiplayer.h \
    ../game/client.h \
    ../game/picturesMapLevel.h \
    ../game/playerparams.h \
    ../game/rangList.h \
    ../game/stopwatch.h \
    ../game/player1.h \
    ../gui/backgroundSetter.h \
    ../gui/musicSetter.h \
    ../gui/mainwindow.h

RESOURCES += \
    ../resources.qrc

TARGET = Test

FORMS +=    \
    ../gui/mainwindow.ui



