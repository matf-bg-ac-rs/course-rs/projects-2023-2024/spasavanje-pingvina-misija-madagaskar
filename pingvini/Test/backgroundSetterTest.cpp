#include "catch.hpp"
#include "../gui/backgroundSetter.h"

TEST_CASE("BackgroundSetter", "[backgroundSetter]") {

    SECTION("Ucitavanje slike") {
        QWidget widget;
        backgroundSetter setter(&widget, ":/resources/background.jpg");

        REQUIRE_NOTHROW(setter.setNewBackground());
    }

    SECTION("Postavljanje pozadine sa nepostojecom slikom") {
        QWidget widget;
        backgroundSetter setter(&widget, "neispravna/putanja/image.jpg");
        setter.setNewBackground();

        REQUIRE(setter.getErrorMessage() == "Failed to load image");
    }
}
