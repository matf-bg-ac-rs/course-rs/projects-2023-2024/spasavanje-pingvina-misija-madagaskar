#include "catch.hpp"
#include "../gui/musicSetter.h"

TEST_CASE("musicSetter()", "[function]")
{
    SECTION("Kada se kreira nova instanca muzika je iskljucena")
    {
        //Arrange
        MusicSetter *music = new MusicSetter();
        bool expected = false;

        //Act
        bool currValue = music->getIsMusic();

        //Assert
        REQUIRE(currValue == expected);

        delete music;
    }
}


TEST_CASE("onMusic", "[function]")
{
    SECTION("Kada se izabere dugme ON krece muzika")
    {
        //Arrange
        MusicSetter *music = new MusicSetter();
        bool expected = true;

        //Act
        music->onMusic();
        bool currValue = music->getIsMusic();

        //Assert
        REQUIRE(currValue == expected);

        delete music;
    }
}

TEST_CASE("offMusic", "[function]")
{
    SECTION("Kada se izabere dugme OFF prekida se muzika")
    {
        //Arrange
        MusicSetter *music = new MusicSetter();
        bool expected = false;

        //Act
        music->offMusic();
        bool currValue = music->getIsMusic();

        //Assert
        REQUIRE(currValue == expected);

        delete music;
    }
}

TEST_CASE("scaleVolume", "[function]")
{
    SECTION("Kada je jacina muzike podesenja na najmanju")
    {
        MusicSetter *music = new MusicSetter();
        double expectedVolume = 0.0;
        int nonScaledValue = 0;

        double scaledVolume = music->scaleVolume(nonScaledValue);

        REQUIRE(scaledVolume == expectedVolume);

        delete music;
    }

    SECTION("Kada je jacina muzike podesenja na najvecu")
    {
        MusicSetter *music = new MusicSetter();
        double expectedVolume = 1.0;
        int nonScaledValue = 99;

        double scaledVolume = music->scaleVolume(nonScaledValue);

        REQUIRE(scaledVolume == expectedVolume);

        delete music;
    }

    SECTION("Kada je jacina muzike podesenja na srednju")
    {
        MusicSetter *music = new MusicSetter();
        double expectedVolume = 0.5;
        int nonScaledValue = 50;

        double scaledVolume = music->scaleVolume(nonScaledValue);

        REQUIRE(qRound(scaledVolume)  == qRound(expectedVolume));

        delete music;
    }
}
