#include "catch.hpp"
#include <QGraphicsScene>
#include "../game/fish.h"



TEST_CASE("fish()", "[function]") {
    SECTION("Postavljanje vrednosti u konstruktoru") {

        Fish* fish = new Fish();

        REQUIRE(fish->getMapLevel() != nullptr);
        REQUIRE(fish->getMusicSetter() != nullptr);

        delete fish;
    }
}

TEST_CASE("collect()", "[function]")
{
    SECTION("Collect metod sklanja ribu sa scene")
    {
        Fish fish;

        QGraphicsScene scene;
        scene.addItem(&fish);
        fish.collect();

        REQUIRE(scene.items().isEmpty());

    }
}
