#include "catch.hpp"
#include "../game/playerparams.h"

TEST_CASE("PlayerParams()", "[function]")
{
    SECTION("Kada se kreira konstruktor screenHeight je pozitivno")
    {
        //Arrange
        PlayerParams *params = new PlayerParams();

        //Act
        params->getScreenHeight();
        qreal value = params->screenHeight;

        //Assert
        REQUIRE(value > 0);

        delete params;
    }

    SECTION("Racuna se visina igraca pomocu visine ekrana")
    {
        //Arrange
        PlayerParams *params = new PlayerParams();
        qreal expectedVal = 70.04;
        params->screenHeight = 824;

        //Act
        params->scaleParams();
        qreal returnVal = params->height;

        //Assert
        REQUIRE(returnVal == expectedVal);

        delete params;
    }

    SECTION("Racuna se sirina igraca pomocu visine igraca")
    {
        //Arrange
        PlayerParams *params = new PlayerParams();
        params->screenHeight = 824;
        qreal expectedVal = 70.04 * 0.4;

        //Act
        params->scaleParams();
        qreal returnVal = params->width;

        //Assert
        REQUIRE(returnVal == expectedVal);

        delete params;
    }

    SECTION("Racuna se korak Y igraca pomocu visine igraca")
    {
        //Arrange
        PlayerParams *params = new PlayerParams();
        qreal expectedVal = 70.04 * 0.1;
        params->screenHeight = 824;

        //Act
        params->scaleParams();
        qreal returnVal = params->stepY;

        //Assert
        REQUIRE(returnVal == expectedVal);

        delete params;
    }

    SECTION("Racuna se gravitacija pomocu koraka igraca")
    {
        //Arrange
        PlayerParams *params = new PlayerParams();
        qreal expectedVal = 70.04 * 0.1 * 0.02;
        params->screenHeight = 824;

        //Act
        params->scaleParams();
        qreal returnVal = params->gravity;

        //Assert
        REQUIRE(returnVal == expectedVal);

        delete params;
    }
}
