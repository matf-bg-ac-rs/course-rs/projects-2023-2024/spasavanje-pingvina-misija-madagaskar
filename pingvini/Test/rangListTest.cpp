#include "catch.hpp"
#include "../game/rangList.h"
#include<QFile>

TEST_CASE("getFilePath", "[function]")
{

    SECTION("Provera postojanja fajla iz kojeg se citaju rezultati")
    {
        //Arrange
        QString path = "../res.txt";
        QFile txt_file(path);

        //Act

        //Assert
        REQUIRE(txt_file.exists());

    }
}

  TEST_CASE("addToRangList", "[function]")  {


    RangList rangList;
    rangList.setFilePath("test_ranglist.txt");

    SECTION("Provera postojanja fajla u koji  se upisuju rezultati")
 {

        QString path = "../res.txt";
        QFile txt_file(path);

        REQUIRE(txt_file.exists());

 }
     SECTION("Testiranje postavljanje putanje")
    {
        QString filePathToSet = "proba_file_path.txt";
        rangList.setFilePath(filePathToSet);

        QString filePathFromGet = rangList.getFilePath();

        REQUIRE(filePathToSet == filePathFromGet);
    }


 }


