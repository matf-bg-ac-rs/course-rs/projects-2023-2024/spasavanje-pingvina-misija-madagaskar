#include "catch.hpp"
#include "../game/player1.h"

TEST_CASE("keyReleaseEvent", "[function]")
{
    SECTION("Otpustanjem dugmeta LEFT brzina igraca postaje 0")
    {
        //Arrange
        Player1 *player = new Player1();
        PlayerParams *params = new PlayerParams();
        qreal expectedVal = 0;

        QKeyEvent *event = new QKeyEvent(QEvent::KeyPress,Qt::Key_Left,Qt::NoModifier);

        //Act
        player->keyReleaseEvent(event);
        qreal currVal = player->getVelocityX();

        //Assert
        REQUIRE(currVal == expectedVal);

        delete player;
        delete params;
        delete event;
    }

    SECTION("Otpustanjem dugmeta RIGHT brzina igraca postaje 0")
    {
        //Arrange
        Player1 *player = new Player1();
        PlayerParams *params = new PlayerParams();
        qreal expectedVal = 0;

        QKeyEvent *event = new QKeyEvent(QEvent::KeyPress,Qt::Key_Right,Qt::NoModifier);

        //Act
        player->keyReleaseEvent(event);
        qreal currVal = player->getVelocityX();

        //Assert
        REQUIRE(currVal == expectedVal);

        delete player;
        delete params;
        delete event;
    }
}


TEST_CASE("keyPressEvent", "[function]")
{
    SECTION("Postavljanje brzine na velicinu koraka pritiskom strelice na desno"){

        Player1 *player = new Player1();
        PlayerParams *parameters = new PlayerParams();
        qreal expectedValue = parameters->stepX;

        QKeyEvent *event = new QKeyEvent(QEvent::KeyPress,Qt::Key_Right,Qt::NoModifier);
        player->keyPressEvent(event);
        qreal currentValue = player->getVelocityX();


        delete player;
        delete parameters;
        delete event;

        REQUIRE(currentValue == expectedValue);
    }
    SECTION("Postavljanje brzine na velicinu koraka pritiskom strelice na levo"){

        Player1 *player = new Player1();
        PlayerParams *parameters = new PlayerParams();
        qreal expectedValue = - parameters->stepX;


        QKeyEvent *event = new QKeyEvent(QEvent::KeyPress,Qt::Key_Left,Qt::NoModifier);
        player->keyPressEvent(event);
        qreal currentValue = player->getVelocityX();


        delete player;
        delete parameters;
        delete event;

        REQUIRE(currentValue == expectedValue);
    }
}



