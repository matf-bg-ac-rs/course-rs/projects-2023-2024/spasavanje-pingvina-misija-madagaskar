#include "catch.hpp"
#include "../game/stopwatch.h"
#include <QtTest>

bool areQTimesEqual(const QTime& lhs, const QTime& rhs) {
    return lhs.msecsSinceStartOfDay() == rhs.msecsSinceStartOfDay();
}

TEST_CASE("Stopwatch počinje i zaustavlja se ispravno", "[stopwatch]") {
    stopwatch mojStopwatch;

    SECTION("Stopwatch počinje") {
        mojStopwatch.start();
        REQUIRE(mojStopwatch.getTimeElapsed() == QTime(0, 0));
    }

    SECTION("Stopwatch zaustavlja") {
        mojStopwatch.start();
        mojStopwatch.stop();
        REQUIRE(mojStopwatch.getTimeElapsed() == QTime(0, 0));
    }
}

TEST_CASE("Manipulacija vremenom na Stopwatch-u", "[stopwatch]") {
    stopwatch mojStopwatch;

    SECTION("Povećanje vremena") {
        mojStopwatch.start();
        mojStopwatch.increaseTime(5);
        REQUIRE(mojStopwatch.getTimeElapsed() == QTime(0, 0, 0, 50));
    }

    SECTION("Smanjenje vremena - ne može biti negativno") {
        mojStopwatch.start();
        mojStopwatch.decreaseTime(10);
        REQUIRE(mojStopwatch.getTimeElapsed() == QTime(0, 0));
    }

    SECTION("Povecavanje vremena")
    {
        mojStopwatch.start();
        mojStopwatch.increaseTime(2);
        mojStopwatch.decreaseTime(1);
        QTime timeAdded = QTime(0, 0, 0, 10);

        REQUIRE(areQTimesEqual(mojStopwatch.getTimeElapsed(),timeAdded));
    }

}


