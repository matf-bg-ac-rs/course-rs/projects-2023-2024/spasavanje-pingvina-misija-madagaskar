#include "catch.hpp"
#include "../game/mapitem.h"

TEST_CASE("mapItem()", "[function]") {
    SECTION("Ispisivanje poruke o gresci kada prosledimo neispravnu putanju") {

        mapItem *item = new mapItem(":/non_existing_image.png");
        QString expectedMessage = "Failed to load image: :/non_existing_image.png";


        REQUIRE(item->getErrorMessage() == expectedMessage);
        delete item;
    }

    SECTION("Proveravanje da li nam je error message prazna ako prosledimo ispravnu putanju")
    {
        QString validImagePath = ":/resources/lemur.png";

        mapItem* item = new mapItem(validImagePath);

        REQUIRE(item->getErrorMessage().isEmpty());
    }
}
