#include "catch.hpp"
#include "../game/picturesMapLevel.h"

TEST_CASE("getPath", "[function]")
{
    SECTION("Kada se posalje simbol L da se vrati slika lemura")
    {
        //Arrange
        picturesMapLevel *picture = new picturesMapLevel();
        QString expectedPath = ":/resources/lemur.png";

        //Act
        QString currentPath = picture->getPath('L');

        //Assert
        REQUIRE(currentPath == expectedPath);

        delete picture;
    }

    SECTION("Kada se posalje simbol G da se vrati slika garda")
    {
        //Arrange
        picturesMapLevel *picture = new picturesMapLevel();
        QString expectedPath = ":/resources/guard.png";

        //Act
        QString currentPath = picture->getPath('G');

        //Assert
        REQUIRE(currentPath == expectedPath);

        delete picture;
    }

    SECTION("Kada se posalje simbol C da se vrati slika kaveza")
    {
        //Arrange
        picturesMapLevel *picture = new picturesMapLevel();
        QString expectedPath = ":/resources/kavez_resize.png";

        //Act
        QString currentPath = picture->getPath('C');

        //Assert
        REQUIRE(currentPath == expectedPath);

        delete picture;
    }

    SECTION("Kada se posalje simbol P da se vrati slika platforme")
    {
        //Arrange
        picturesMapLevel *picture = new picturesMapLevel();
        QString expectedPath = ":/resources/platform_resized4.png";

        //Act
        QString currentPath = picture->getPath('P');

        //Assert
        REQUIRE(currentPath == expectedPath);

        delete picture;
    }

    SECTION("Kada se posalje simbol H da se vrati slika rupe")
    {
        //Arrange
        picturesMapLevel *picture = new picturesMapLevel();
        QString expectedPath = ":/resources/holeNewest.png";

        //Act
        QString currentPath = picture->getPath('H');

        //Assert
        REQUIRE(currentPath == expectedPath);

        delete picture;
    }

    SECTION("Kada se posalje simbol K da se vrati slika kljuca")
    {
        //Arrange
        picturesMapLevel *picture = new picturesMapLevel();
        QString expectedPath = ":/resources/key_resized.png";

        //Act
        QString currentPath = picture->getPath('K');

        //Assert
        REQUIRE(currentPath == expectedPath);

        delete picture;
    }

    SECTION("Kada se posalje simbol $ da se vrati slika igraca")
    {
        //Arrange
        picturesMapLevel *picture = new picturesMapLevel();
        QString expectedPath = ":/resources/player_resized.png";

        //Act
        QString currentPath = picture->getPath('$');

        //Assert
        REQUIRE(currentPath == expectedPath);

        delete picture;
    }

    SECTION("Kada se posalje simbol F da se vrati slika ribe")
    {
        //Arrange
        picturesMapLevel *picture = new picturesMapLevel();
        QString expectedPath = ":/resources/fishResized.png";

        //Act
        QString currentPath = picture->getPath('F');

        //Assert
        REQUIRE(currentPath == expectedPath);

        delete picture;
    }
    SECTION("Kada se posalje simbol koji ne postoji u mapi ona vrati prazan string")
    {
        picturesMapLevel mapLevel;

        QString path = mapLevel.getPath('X');

        REQUIRE(path.isEmpty());
    }
}
