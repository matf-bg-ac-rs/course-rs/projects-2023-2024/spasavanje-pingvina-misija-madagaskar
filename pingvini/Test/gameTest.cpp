#include "catch.hpp"
#include "../game/game.h"
#include "../gui/mainwindow.h"

TEST_CASE("Game inicijalizacija", "[Game]") {
    SECTION("Scena, pogled, igrac i stoperica su ispravno inicijalizovani") {
        QString player1 = "player1";
        MainWindow* main = new MainWindow();

        Game* game = new Game(player1, main);

        REQUIRE(game->scene != nullptr);
        REQUIRE(game->view != nullptr);
        REQUIRE(game->getStopwatch() != nullptr);
        REQUIRE(game->player != nullptr);
    }
}

TEST_CASE("setSceneBackground", "[function]")
{
    SECTION("Postavljanje vrednosti za pozadinu")
    {
        QString player1 = "player1";
        MainWindow* main = new MainWindow();
        Game* game = new Game(player1, main);

        game->setSceneBackground();

        REQUIRE(game->scene->sceneRect() == QRectF(0, 0, 1200, 600));
        REQUIRE(game->backgroundBrush().texture().toImage() == QImage(":/resources/newBackg.jpg"));
        REQUIRE(game->horizontalScrollBarPolicy() == Qt::ScrollBarAlwaysOff);
        REQUIRE(game->verticalScrollBarPolicy() == Qt::ScrollBarAlwaysOff);
    }
}

TEST_CASE("setView", "[function]")
{
    SECTION("Postavljanje vrednosti za pogled")
    {
        QString player1 = "player1";
        MainWindow* main = new MainWindow();
        Game* game = new Game(player1, main);

        game->setView();

        REQUIRE(game->view != nullptr);
        REQUIRE(game->view->sceneRect() == QRectF(0, 0, 600, 500));
        REQUIRE(game->view->size() == QSize(600, 500));
    }
}

TEST_CASE("Game viewChange method", "[Game]")
{
    SECTION("Pozicija je manja od 0")
    {
        QString player1 = "player1";
        MainWindow* main = new MainWindow();
        Game* game = new Game(player1, main);

        game->player->setPos(-100, 0);


        game->viewChange();

        REQUIRE(game->getLabel()->pos() == QPoint(-110, 10));
    }
    SECTION("Pozicija igraca veca od 800")
    {
        QString player1 = "player1";
        MainWindow* main = new MainWindow();
        Game* game = new Game(player1, main);

        game->player->setPos(900, 0);


        game->viewChange();

        REQUIRE(game->getLabel()->pos() == QPoint(890, 10));
    }
    SECTION("Pozicija igraca veca je izmedju 0 i  800")
    {
        QString player1 = "player1";
        MainWindow* main = new MainWindow();
        Game* game = new Game(player1, main);

        game->player->setPos(400, 0);


        game->viewChange();

        REQUIRE(game->getLabel()->pos() == QPoint(390, 10));
    }
}

