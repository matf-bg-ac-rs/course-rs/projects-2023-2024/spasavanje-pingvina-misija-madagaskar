#include "catch.hpp"
#include "../game/lemur.h"
#include "../game/picturesMapLevel.h"


TEST_CASE("lemur()", "[function]"){

    SECTION("Inicijalne vrednosti parametara pri kreiranju novog lemura") {
        Lemur *lemur = new Lemur();
        int expectedDirection = 1;
        qreal expectedDistance = 50;
        qreal expectedCurrentDistance = 0;


        REQUIRE(lemur->getDirection() == expectedDirection);
        REQUIRE(lemur->getDistance() == expectedDistance);
        REQUIRE(lemur->getCurrentDistance() == expectedCurrentDistance);


        delete lemur;


    }
}

TEST_CASE("move" , "[function]"){

    SECTION("Do prvih 24 poziva funkcije  lemur se kreće u desno"){
        Lemur *lemur = new Lemur();
        qreal initialX = lemur->getX();


        for (int i = 0; i < 24; i++) {
            lemur->move();
            qreal newX = lemur->getX();

            REQUIRE(initialX < newX);
            initialX = newX;
        }

        delete lemur;

}

    SECTION("Od 25 do 49 poziva funkcije  lemur se kreće u levo"){
        Lemur *lemur = new Lemur();
        qreal initialX = lemur->getX();


        for (int i = 0; i < 50; i++) {
            lemur->move();
            qreal newX = lemur->getX();
            if (i > 25){
                REQUIRE(initialX > newX);

        }

         initialX = newX;
    }

        delete lemur;

}

    SECTION("Na svakih 50 poziva funkcije  lemur se vraca u pocetni polozaj"){
        Lemur *lemur = new Lemur();
        qreal initialX = lemur->getX();
        qreal newX;


        for (int i = 0; i < 50; i++) {
        lemur->move();
        newX = lemur->getX();
     }

        REQUIRE(initialX == newX);


        delete lemur;

}


}
