#include "./gui/mainwindow.h"

#include <QApplication>
#include <QLabel>
#include <QIcon>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    a.setWindowIcon(QIcon(":/resources/pingvin2.png"));

    w.show();

    return a.exec();
}
