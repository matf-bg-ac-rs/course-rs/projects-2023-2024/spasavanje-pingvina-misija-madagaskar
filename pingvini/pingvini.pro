QT       += core gui multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++20


QMAKE_CXXFLAGS += -g
QMAKE_LFLAGS_DEBUG += -g


SOURCES += \
    ./game/level.cpp \
    ./main.cpp \
    ./gui/mainwindow.cpp \
    ./game/mapitem.cpp \
    ./game/picturesMapLevel.cpp \
    ./game/player1.cpp \
    game/client.cpp \
    game/fish.cpp \
    game/game.cpp \
    game/key.cpp \
    game/lemur.cpp \
    game/multiplayer.cpp \
    game/playerparams.cpp \
    game/stopwatch.cpp \
    gui/backgroundSetter.cpp \
    game/rangList.cpp \
    gui/musicSetter.cpp

HEADERS += \
    ./gui/mainwindow.h \
    ./game/mapitem.h \
    ./game/picturesMapLevel.h \
    ./game/player1.h \
    ./game/level.h \ \
    game/client.h \
    game/fish.h \
    game/game.h \
    game/key.h \
    game/lemur.h \
    game/multiplayer.h \
    game/playerparams.h \
    game/stopwatch.h \
    gui/backgroundSetter.h \
    game/rangList.h \
    gui/musicSetter.h

FORMS += \
    ./gui/mainwindow.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
DISTFILES += \
    ../../../../Lightfox - Pluck (Official Music Video).wav \
    ../Desktop/pingvini/pingvinite-ot-madagaskar-blu-ray-31.png \
    Lightfox - Pluck (Official Music Video).wav \
    Lightfox - Pluck (Official Music Video).wav \
    res.txt \
    res_songs/Lightfox - Pluck (Official Music Video).wav \
    res_songs/Lightfox - Pluck (Official Music Video).wav \
    res_songs/Lightfox - Pluck (Official Music Video).wav \
    resources/fish.png

SUBDIRS += \
    ../server/server.pro
