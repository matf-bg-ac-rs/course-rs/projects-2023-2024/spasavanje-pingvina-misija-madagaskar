#include "server.h"


Server::Server(QObject *parent) : QObject(parent)
{
    server = new QTcpServer(this);
    client1 = nullptr;
    client2 = nullptr;

    connect(server, &QTcpServer::newConnection, this, &Server::connection);

    server->listen(QHostAddress::AnyIPv4, 12345);

    qDebug()<<"Server je pokrenut!";
}

void Server::connection() {

    QTcpSocket* soket = server->nextPendingConnection();

    if(soket) {
        if(client1 == nullptr) {
            client1 = soket;
            client1->write("Client 1");

        }
        else if(client2==nullptr){
            client2 = soket;
            client2->write("Client 2");
        }
        else {
            qDebug() << "Odbijena veza: Previše klijenata";
            soket->disconnectFromHost();
            soket->deleteLater();
            return;
        }

        connect(soket, &QTcpSocket::readyRead, this, &Server::communication);
        connect(soket, &QTcpSocket::disconnected, this, &Server::disconnection);

    }
    if(client1 && client2){

        client1->write("Both are connected!");
        client2->write("Both are connected!");
    }

    soket->flush();
    soket->waitForBytesWritten();
}

void Server::disconnection() {
    client1 = nullptr;
    client2 = nullptr;
}

void Server::communication() {
    QTcpSocket* sender_ = dynamic_cast<QTcpSocket*>(sender());

    if(sender_){
        QByteArray data = sender_->readAll();
        if (client1 && client2) {
            if (sender_ == client1) {
                if (!client2->write(data)) {
                    qDebug() << "Greška pri slanju podataka klijentu 2";
                }
            } else {
                if (!client1->write(data)) {
                    qDebug() << "Greška pri slanju podataka klijentu 1";
                }
            }
        }
    }
}

Server::~Server() {
    delete server;
}
