#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include<QDebug>

class Server : public QObject
{
    Q_OBJECT
public:
    Server(QObject* parent = nullptr);
    ~Server();

public slots:
    void connection();
    void disconnection();
    void communication();

private:
    QTcpServer* server;
    QTcpSocket* client1;
    QTcpSocket* client2;
};

#endif // SERVER_H
